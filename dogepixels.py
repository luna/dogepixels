#!/usr/bin/env python3

# pixels.py - graphing the amount of pixels in an image

from collections import Counter
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

MOST_COMMON = 700

def to_hex(tup) -> str:
    """Convert rgb tuple to a hex value"""
    return '#%02x%02x%02x' % tup


def to_col(hexval) -> tuple:
    # https://stackoverflow.com/a/29643643
    raw_rgb = tuple(int(hexval[i:i+2], 16) for i in (0, 2 ,4))

    return tuple(component / 255 for component in raw_rgb)


def main(file):
    full_path = Path.resolve(Path(file))
    img = Image.open(full_path)
    rgb_img = img.convert('RGB')

    pixels = Counter()

    print('total resolution', img.width, img.height)

    for val_x in range(img.width):
        for val_y in range(img.height):
            rgb_tup = rgb_img.getpixel((val_x, val_y))

            # convert it to hex value, then insert into counter
            pixels[to_hex(rgb_tup)] += 1

    img.close()

    print('done! making graph')
    print(pixels.most_common(10))

    labels, values = zip(*pixels.most_common(MOST_COMMON))

    labels = sorted(labels)
    values = [pixels[l] for l in labels]

    indexes = np.arange(len(labels))
    width = 1

    plt.xlabel('hex color code')
    plt.ylabel('amount of pixels')

    plt.bar(indexes, values, width, color=[to_col(c[1:]) for c in labels])
    plt.xticks(np.arange(0, len(labels), step=50), labels)

    plt.title(f'{MOST_COMMON} most common colors for {full_path.name}')
    plt.show()

if __name__ == '__main__':
    import argparse
    import sys

    parser = argparse.ArgumentParser(description="Plot pixels.")
    parser.add_argument("file", help="The picture to be processed")

    args = parser.parse_args()
    sys.exit(main(args.file))
